
let num1 = 25;
let num2 = 5;
console.log("The result of num1 + num2 should be 30.");
console.log("Actual Result:");
console.log(num1 + num2);

let num3 = 156;
let num4 = 44;
console.log("The result of num3 + num4 should be 200.");
console.log("Actual Result:");
console.log(num3 + num4);

let num5 = 17;
let num6 = 10;
console.log("The result of num5 - num6 should be 7.");
console.log("Actual Result:");
console.log(num5-num6);


let minutesHour = 60;
let hoursDay = 24;
let daysWeek = 7;
let weeksMonth = 4;
let monthsYear = 12;
let daysYear = 365;
let resultMinutes = minutesHour * hoursDay * daysYear;
console.log('There are ' + resultMinutes + ' minutes in a year.');

let tempCelsius = 132;
let resultFahrenheit = tempCelsius * 9 / 5 + 32;
console.log(tempCelsius + ' degrees Celcius when converted to Fahrenheit is ' + resultFahrenheit);

let num7 = 165;
let num = 8;
let remainder = num7 % num;
console.log('The remainder of ' + num7 + ' divided by ' + num + ' is: ' + remainder);
//Log the value of the remainder in the console.

let isDivisibleBy8 = false;
console.log("Is num7 divisible by 8?");
console.log(isDivisibleBy8);
//Log the value of isDivisibleBy8 in the console.

let num8 = 348;
let num9 = 4;
let theRemainder = num8 % num9;
console.log('The remainder of ' + num8 + ' divided by ' + num9 + ' is: ' + theRemainder);
//Log the value of the remainder in the console.
let isDivisibleBy4 = true;
console.log("Is num8 divisible by 4?");
console.log(isDivisibleBy4);
//Log the value of isDivisibleBy4 in the console.